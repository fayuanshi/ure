/*
 * @Author: FayuanShi
 * @Date: 2021-06-15 21:44:30
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-06-15 22:09:44
 * @Description: file content
 */
module.exports = async (req, res, next) => {
    const index = Number(req.query.pageNum)
    const limit = Number(req.query.pageSize)
    req.newQuery = {}
    for (let key in req.query) {
        if (key !== 'pageNum' && key !== 'pageSize' &&req.query[key]!=='' ) {
            req.newQuery[key] = req.query[key]
        }
    }
    console.log(req.newQuery);
    if (req.query.pageNum && req.query.pageSize) {


        req.index = index
        req.offset = (index - 1) * limit
        req.limit = limit
        console.log(req.offset, req.limit)
        next()
    } else {
        req.offset = null
        req.limit = null
        next()
    }


}