/*
 * @Author: FayuanShi
 * @Date: 2021-06-15 21:03:34
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-06-15 21:13:19
 * @Description: file content
 */
module.exports = async (req,res,next)=>{
    const path = require('path');
    const { readdir } = require('fs/promises')

    let modelPath = path.join(__dirname, '../models')
    const modelName = req.params.model
    req.modelName = modelName



    async function mount(mountPath){
        const dirList = await readdir(mountPath)
        for(const item of dirList){
            if(item.substring(item.length-3) ==='.js'  ){
                if(item.substring(0,item.length-3)=== modelName){
                    const modelFullPath = path.join(mountPath,item)
                    req.Model = require(modelFullPath)
                    
                }
             
            }else{
                const dirPath = path.join(mountPath,item)
                await mount(dirPath)
            }
        }
  
    } 

    await mount(modelPath)


    next()
}