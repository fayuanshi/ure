
module.exports = async (req,res,next)=>{
    const jwt = require('jsonwebtoken')
    const User = require('../models/system/user')

    
    const raw = String(req.headers.authorization).split(' ').pop()


    if(typeof(raw) == "undefined"){
        return    res.send({
            code:500,
            msg:'token丢失'
        })
        
    }
    try{
        const tokenData =  jwt.verify(raw,'12345')
        const {id} = tokenData
      
        user = await User.findByPk(id)
        req.user = user.dataValues

        next()
    }catch(err){
        return res.send({
            code:500,
            msg:err
        })
    }    
    

    
}