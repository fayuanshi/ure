module.exports = (app) => {
    const express = require('express')
    const router = express.Router()
    const{auth} = app.locals.middleware

    let multer = require('multer')
    let uploadPath = require('../../utils/getUploadPath')(app)

    //定义下载中间件
    let upload = multer({
        storage: multer.diskStorage({
            destination:uploadPath,
            filename: function (req, file, cb) {
                let changedName = (new Date().getTime()) + '-' + file.originalname;           
                changedName = String(changedName)
                cb(null, changedName);
                
                req[`changedName`] =changedName

                
                       
            }
        })
    });



    router.post('/upload', upload.single('file'), function (req, res, next) {
        console.log(req.file)
        let path = req.file.destination.replace(app.locals.uploadPath,"")+'/'+req.changedName

        res.json({
            code: '200',
            type: 'single',
            originalname: req.file.originalname,
            path: path
        })
    
    })

    app.use('/', auth,router)
}