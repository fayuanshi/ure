

module.exports = (app) => {
    const express = require('express')
    const router = express.Router()

    const auth = app.locals.middleware.auth

    const deptService = require('../../services/system/deptService')
    const Dept = require('../../models/system/dept')


    router.get('/treeselect', async (req, res) => {

        const deptList = await Dept.findAll({
            where: {
                delFlag: 0
            }
        })
        const deptsTree = deptService.buildDeptTree(deptList)
        res.send({
            code: 200,
            data: deptsTree
        })





    })

    router.get('/list/exclude/:deptId', async (req, res) => {
        let deptList = await Dept.findAll({
            where: {
                delFlag: 0
            }
        })
        deptList = JSON.parse(JSON.stringify(deptList))
        for (let i = 0; i < deptList.length; i++) {
            if (deptList[i].deptId == req.params.deptId) {
                deptList.splice(i, 1)
            }
        }

        res.send({
            code: 200,
            data: deptList
        })
    })

    router.post('/', async (req, res) => {

        let ancestors = await deptService.buildAncestors(req.body.parentId)
        console.log(ancestors);
        ancestors = ancestors + ',' + req.body.parentId
        req.body['ancestors'] = ancestors
        await Dept.create(req.body)
        res.send({
            code: 200,
            msg: "操作成功"
        })
    })

    router.put('/', async (req, res) => {
        console.log(req.body);
        if(req.body.deptId != 100){
            let ancestors = await deptService.buildAncestors(req.body.parentId)
            ancestors = ancestors + ',' + req.body.parentId
            req.body['ancestors'] = ancestors
        }
        await Dept.update(req.body, {
            where: {
                deptId: req.body.deptId
            }
        })
        res.send({
            code: 200,
            msg: "操作成功"
        })
    })

    router.delete('/:deptId', async (req, res) => {
        const deptId = req.params.deptId
        const depts = await Dept.findAll({
            where: {
                parentId: deptId,
                delFlag: 0
            }
        })

        if (depts.length > 0) {
            res.send({
                code: 500,
                msg: '不允许删除有子部门的部门'
            })
        } else {
            await Dept.update({ delFlag: 2 }, {
                where: {
                    deptId,
                }
            })
            res.send({
                code: 200,
                msg: '操作成功'
            })
        }

    })





    app.use('/system/dept', auth, router)


}