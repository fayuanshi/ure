module.exports = (app) => {
    const express = require('express')
    const router = express.Router()
    const { Op } = require('sequelize')
    const sequelize = require('../../framework/db')

    const auth = app.locals.middleware.auth

    const { Menu, RoleMenu } = app.locals.models

    const menuService = require('../../services/system/menuService')
    const menuDal = require('../../dal/system/menuDal')


    router.get('/list', async (req, res) => {
        console.log(req.query)
        let menus = await Menu.findAll({
            where:req.query
        })

        menus = JSON.parse(JSON.stringify(menus))
        for (let i = 0; i < menus.length; i++) {
            menus[i]['children'] = []
            menus[i]['params'] = {}
            menus[i]['parentName'] = null
            menus[i]['searchValue'] = null
            menus[i]['isCache'] = String(menus[i]['isCache'])
            menus[i]['isFrame'] = String(menus[i]['isFrame'])
            menus[i]['orderNum'] = String(menus[i]['orderNum'])
        }


        res.send({
            code: 200,
            data: menus
        })
    })

    router.delete('/:menuId', async (req, res) => {
        const menuIds = req.params.menuId.split(',')
        for (let i = 0; i < menuIds.length; i++) {
            let menus = await Menu.findAll({
                where: {
                    parentId: menuIds[i],
                    menuType: {
                        [Op.or]: ["M", "C"],
                    }
                }
            })
            if (menus.length > 0) {
                res.send({
                    code: 500,
                    msg: "存在子菜单，不允许删除"
                })
            }
        }

        try {
            const result = await sequelize.transaction(
                async (t) => {
                    menuIds.forEach((item) => {
                        Menu.destroy({
                            where: {
                                menuId: item
                            }
                        })
                        RoleMenu.destroy({
                            where: {
                                menuId: item
                            }
                        })
                    })

                });

            res.send({
                code: 200,
                msg: '操作成功'

            })

        } catch (error) {
            console.log(error);

            res.send({
                code: 500,
                msg: error
            })

        }
    })

    router.get('/treeselect', async (req, res) => {
        let menus = await Menu.findAll()
        menus = JSON.parse(JSON.stringify(menus))

        const tree = await menuService.getTreeselect(menus)

        res.send({
            code: 200,
            data: tree
        })

    })

    router.get('/roleMenuTreeselect/:roleId', async (req, res) => {
        let menus = await menuDal.selectMenuList()
        menus = JSON.parse(JSON.stringify(menus))

        const treeselect = await menuService.getTreeselect(menus)
        const checkedKeys = await menuDal.selectMenuListByRoleId(req.params.roleId, true)
        res.send({
            checkedKeys,
            code: 200,
            menus: treeselect
        })



    })









    app.use('/system/menu', auth, router)


}