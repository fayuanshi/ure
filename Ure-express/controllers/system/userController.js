module.exports = (app) => {
	const express = require('express')
	const { Op } = require("sequelize");
	const bcrypt = require('bcrypt');
	const router = express.Router()

	const sequelize = require('../../framework/db')
	const auth = app.locals.middleware.auth
	const roleService = require('../../services/system/roleService')

	const { Post, Role, User, UserRole, UserPost } = app.locals.models

	router.get('/', async (req, res) => {
		const posts = await Post.findAll()
		const roles = await Role.findAll({
			where: {
				roleId: {
					[Op.ne]: 1
				},
				del_flag: 0
			}
		})
		res.send({
			code: 200,
			posts,
			roles,
		})
	})

	router.get('/:id', async (req, res) => {

		let user = await User.findByPk(req.params.id)
		user = JSON.parse(JSON.stringify(user))

		user = await roleService.getUser(user)
		const posts = await Post.findAll()

		let roles
		if (req.params.id == 1) {
			roles = await Role.findAll({
				where: {
					del_flag: 0
				}
			})

		} else {
			roles = await Role.findAll({
				where: {
					roleId: {
						[Op.ne]: 1
					},
					del_flag: 0
				}
			})
		}
		let postIds = [],
			roleIds = []
		let userPosts = await UserPost.findAll({
			where: {
				userId: req.params.id
			}
		})
		userPosts = JSON.parse(JSON.stringify(userPosts))
		userPosts.forEach((item) => {
			postIds.push(item.postId)
		})

		let userRoles = await UserRole.findAll({
			where: {
				userId: req.params.id
			}
		})
		userRoles = JSON.parse(JSON.stringify(userRoles))
		userRoles.forEach((item) => {
			roleIds.push(item.roleId)
		})

		res.send({
			code: 200,
			data: user,
			posts,
			roles,
			roleIds,
			postIds
		})
	})


	router.post('/', async (req, res) => {
		let users = await User.findAll({
			where:{
				userName:req.body.userName
			}
		})
		

		if(Array.isArray(users)){
			if(users.length>0){
				return res.send({
					code:500,
					msg:'该用户名已存在'
				})
			}
		}
		let date = new Date()
		req.body['createTime'] = date
		req.body['createBy'] = req.user.userName


		req.body.password = bcrypt.hashSync(req.body.password, 5)

		try {
			const result = await sequelize.transaction(
				async (t) => {
					const user = await User.create(req.body, { transaction: t })

					for (let i = 0; i < req.body.postIds.length; i++) {
						let userPost = await UserPost.create({
							userId: user.userId,
							postId: req.body.postIds[i]
						}, { transaction: t })
					}
					for (let i = 0; i < req.body.roleIds.length; i++) {
						let userPost = await UserRole.create({
							userId: user.userId,
							roleId: req.body.roleIds[i]
						}, { transaction: t })
					}

				});
			res.send({
				code: 200,
				msg: '操作成功'

			})

		} catch (error) {

			res.send({
				code: 500,
				msg: error
			})

		}
	})

	router.put('/', async (req, res) => {
		let users = await User.findAll({
			where:{
				userName:req.body.userName
			}
		})
		

		if(Array.isArray(users)){
			if(users.length>0){
				return res.send({
					code:500,
					msg:'该用户名已存在'
				})
			}
		}
		let date = new Date()
		req.body['updateTime'] = date
		req.body['updateBy'] = req.user.userName
		try {
			const result = await sequelize.transaction(
				async (t) => {
					const user = await User.findByPk(req.body.userId, { transaction: t })
					await user.update(req.body, { transaction: t })
					await UserPost.destroy({
						where: {
							userId: Number(req.body.userId)
						},
						transaction: t
					})
					await UserRole.destroy({
						where: {
							userId: Number(req.body.userId)
						},
						transaction: t
					})
					for (let i = 0; i < req.body.postIds.length; i++) {
						let userPost = await UserPost.create(
							{
								userId: user.userId,
								postId: req.body.postIds[i]
							},
							{ transaction: t }
						)
					}
					for (let i = 0; i < req.body.roleIds.length; i++) {
						let userPost = await UserRole.create(
							{
								userId: user.userId,
								roleId: req.body.roleIds[i]
							},
							{ transaction: t }
						)
					}


				});
			res.send({
				code: 200,
				msg: '操作成功'

			})

		} catch (error) {

			res.send({
				code: 500,
				msg: error
			})

		}

	})

	router.delete('/:userId', async (req, res) => {
		let users = req.params.userId.split(',')
		try {
			const result = await sequelize.transaction(
				async (t) => {
					for (let i = 0; i < users.length; i++) {
						User.update({ delFlag: 2 }, {
							where: {
								userId: Number(users[i])
							}
						},
							{ transaction: t })
					}
					for (let i = 0; i < users.length; i++) {
						UserPost.destroy({
							where: {
								userId: Number(users[i])
							}
						},
							{ transaction: t })
					}
					for (let i = 0; i < users.length; i++) {
						UserRole.destroy({
							where: {
								userId: Number(users[i])
							}
						},
							{ transaction: t })
					}



				});
			res.send({
				code: 200,
				msg: '操作成功'

			})

		} catch (error) {

			res.send({
				code: 500,
				msg: error
			})

		}



	})

	router.put('/changeStatus', async (req, res) => {
		const user = await User.findByPk(req.body.userId)
		await user.update({
			status: req.body.status
		})

		res.send({
			code: 200,
			msg: "操作成功"
		})
	})

	router.put('/resetPwd', async (req, res) => {
		console.log(req.body)
		hashPassword = bcrypt.hashSync(req.body.password, 5)
		const user = await User.findByPk(req.body.userId)
		await user.update({
			password: hashPassword
		})

		res.send({
			code: 200,
			msg: '修改成功'
		})

	})


	app.use('/system/user', auth, router)
}