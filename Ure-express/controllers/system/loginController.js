module.exports = (app) => {
    const express = require('express')
    const bcrypt = require('bcrypt');
    const jwt = require('jsonwebtoken')

    const router = express.Router()

    const {auth} = app.locals.middleware
    const {User,UserRole} = app.locals.models
    const roleService = require('../../services/system/roleService')
    const menuService = require('../../services/system/menuService')
    const menuDal = require('../../dal/system/menuDal') 


 
    router.post('/login', async (req, res) => {
        const user = await User.findOne({
            where: {
                user_name: req.body.username
            }
        })
        bcrypt.compare(req.body.password, user.password, function (err, result) {
            if (result == true) {
                const token = jwt.sign({  
                        id: String(user.userId)
                    },
                    "12345"
                )

                res.send({
                    code: 200,
                    msg: '登录成功',
                    data: {
                        token,
                    }

                })


            } else {
                res.send({
                    code: 500,
                    msg: "密码无效"
                })
            }
        });

    })

    router.get('/getInfo', auth, async (req, res) => {
        const user = await roleService.getUser(req.user)
        const roles = []
        user.roles.forEach(role => {
            roles.push(role.roleKey)
            if (role.roleKey === 'admin') {
                user.admin = true
            }
        });
        const permissions = await menuService.getPerms(user)
        res.send({
            user,
            roles,
            permissions
        })

    })

    router.get('/getRouters', auth, async (req, res) => {
        let menus
        if (req.user.userId == 1) {
             menus = await menuDal.selectMenuTreeAll()
        }else{
            let userRoles = await UserRole.findAll({
                where:{
                    userId:req.user.userId
                }
            })
            userRoles = JSON.parse(JSON.stringify(userRoles))
            let roleList = []
            for(let i =0 ;i<userRoles.length;i++){
                roleList.push(userRoles[i].roleId)
            }
             menus = await menuDal.selectMenuTreeByRoleId(roleList)
            
        }
      
        const routers = menuService.buildMenus(menus)
        res.send({
            code: 200,
            data: routers,
            msg: "操作成功"
        })

    })



    app.use('/', router)


}