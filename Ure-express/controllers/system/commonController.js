module.exports = (app) => {
    const express = require('express')
    const router = express.Router()
    //登录验证中间件,model挂载中间件，分页中间件
    const { auth, mount, paging } = app.locals.middleware


    //通用分页列表查询接口
    router.get('/list', paging, async (req, res) => {
        const { count, rows } = await req.Model.findAndCountAll({
            where: req.newQuery,
            offset: req.offset,
            limit: req.limit
        });

        res.send({
            code: 200,
            total: count,
            limit: req.limit,
            index: req.index,
            rows: rows

        })

    })

    //通用单条查询
    router.get('/:id', async (req, res) => {
        const row = await req.Model.findByPk(req.params.id)
        if (!row) {
            return res.send({
                code: 500,
                msg: "所修改数据不存在"
            })
        }
        res.send({
            code: 200,
            data: row,
            msg: '查询成功'
        })
    })


    //通用创建接口
    router.post('/', async (req, res) => {
        if('createTime' in req.Model.tableAttributes){
            let date = new Date()
            req.body['createTime'] = date
            req.body['createBy'] = req.user.userName
        }

        const row = await req.Model.create(req.body)

        res.send({
            code: 200,
            msg: '新增成功'
        })
    })



    //通用修改接口
    router.put('/', async (req, res) => {
        let modelIdName = req.modelName + 'Id'
        const row = await req.Model.findByPk(req.body[modelIdName])
        
        if (!row) {
            return res.send({
                code: 500,
                msg: "所修改数据不存在"
            })
        }
        if('updateTime' in req.Model.tableAttributes){
            let date = new Date()
            req.body['updateTime'] = date
            req.body['updateBy'] = req.user.userName
        }

        await row.update(req.body)

        res.send({
            code: 200,
            msg: '修改成功'
        })


    })
    //通用删除接口
    router.delete('/:id', async (req, res) => {

        const list = req.params.id.split(',')
        let modelIdName = req.modelName + 'Id'

        for (let i = 0; i < list.length; i++) {
            await req.Model.destroy({
                where: {
                    [modelIdName]: Number(list[i])
                }
            })

        }

        res.send({
            code: 200,
            msg: '操作成功'
        })
    })






    app.use('/common/:model', auth, mount, router)
}