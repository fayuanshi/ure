module.exports = (app) => {
    const express = require('express')
    const router = express.Router()
    const auth = app.locals.middleware.auth
    const sequelize = require('../../framework/db')

    const { Role, RoleMenu, UserRole } = app.locals.models

    router.get('/:roId', async (req, res) => {
        let role = await Role.findByPk(req.params.roId)
        role = JSON.parse(JSON.stringify(role))
        if (role.deptCheckStrictly == 1) {
            role.deptCheckStrictly = true
        } else if (role.deptCheckStrictly == 0) {
            role.deptCheckStrictly = false
        }
        if (role.menuCheckStrictly == 1) {
            role.menuCheckStrictly = true
        } else if (role.menuCheckStrictly == 0) {
            role.menuCheckStrictly = false
        }

        res.send({
            code: 200,
            data: role
        })
    })

    router.post('/', async (req, res) => {
        let date = new Date()
        req.body['createTime'] = date
        req.body['createBy'] = req.user.userName


        try {
            const result = await sequelize.transaction(
                async (t) => {
                    let role = await Role.create(req.body,
                        { transaction: t })
                    role = JSON.parse(JSON.stringify(role))
                    req.body.menuIds.forEach((item) => {
                        RoleMenu.create(
                            {
                                roleId: role.roleId,
                                menuId: item
                            }
                        )
                    })

                });

            res.send({
                code: 200,
                msg: '操作成功'

            })

        } catch (error) {
            console.log(error);

            res.send({
                code: 500,
                msg: error
            })

        }
    })

    router.put('/', async (req, res) => {
        let date = new Date()
        req.body['updateTime'] = date
        req.body['updateBy'] = req.user.userName
        try {
            const result = await sequelize.transaction(
                async (t) => {
                    let role = await Role.update(req.body, {
                        where: {
                            roleId: req.body.roleId
                        },
                        transaction: t
                    })
                    role = JSON.parse(JSON.stringify(role))

                    await RoleMenu.destroy({
                        where: {
                            roleId: req.body.roleId
                        },
                        transaction: t
                    })

                    for (let i = 0; i < req.body.menuIds.length; i++) {
                        await RoleMenu.create(
                            {
                                roleId: req.body.roleId,
                                menuId: req.body.menuIds[i]
                            },
                            { transaction: t }
                        )
                    }

                });

            res.send({
                code: 200,
                msg: '操作成功'

            })

        } catch (error) {
            console.log(error);

            res.send({
                code: 500,
                msg: error
            })

        }

    })

    router.delete('/:roleId', async (req, res) => {
        console.log(req.params.roleId);
        const roleIds = req.params.roleId.split(',')
        console.log(roleIds);
        try {
            const result = await sequelize.transaction(
                async (t) => {
                    roleIds.forEach((item) => {
                        Role.update({ delFlag: 2 }, {
                            where: {
                                roleId: item
                            }
                        })
                        RoleMenu.destroy({
                            where: {
                                roleId: item
                            }
                        })
                        UserRole.destroy({
                            where: {
                                roleId: item
                            }
                        })
                    })

                });

            res.send({
                code: 200,
                msg: '操作成功'

            })

        } catch (error) {
            console.log(error);

            res.send({
                code: 500,
                msg: error
            })

        }
    })








    app.use('/system/role', auth, router)


}