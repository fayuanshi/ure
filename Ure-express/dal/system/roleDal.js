const sequelize = require('../../framework/db')
const Role = require('../../models/system/role')


module.exports = {
    //通过roleId查询该行数据
    async selectRoleById(roleId){
        const [results,metadata] = await sequelize.query(`  
        select * from sys_role WHERE role_id = ${roleId}
        `,{
            model: Role,
            mapToModel: true
          }
        )

        return results
    },
    //通过userId查询其所有的role
    async selectRoleListByUserId(userId){
        const [results,metadata] = await sequelize.query(`
        select r.role_id
        from sys_role r
	        left join sys_user_role ur on ur.role_id = r.role_id
	        left join sys_user u on u.user_id = ur.user_id
	    where u.user_id = ${userId} and r.del_flag = 0
        `
        )

        return results
    }
    
}