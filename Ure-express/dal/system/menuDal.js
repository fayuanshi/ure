const sequelize = require('../../framework/db')
const Menu = require('../../models/system/menu')
const { QueryTypes } = require('sequelize');

module.exports = {
    //查询所有菜单
    async selectMenuTreeAll() {
        const results = await sequelize.query(`
        select distinct m.menu_id, m.parent_id, m.menu_name, m.path, m.component, m.visible, m.status, ifnull(m.perms,'') as perms, m.is_frame, m.is_cache, m.menu_type, m.icon, m.order_num, m.create_time
		from sys_menu m 
        where m.menu_type in ('M', 'C') and m.status = 0
		order by m.parent_id, m.order_num`, {
            model: Menu,
            type: QueryTypes.SELECT,
            mapToModel: true,
            
        });

        return results

    },
    async selectMenuTreeByRoleId(roleIdList) {

        const results = await sequelize.query(`
        SELECT
        distinct m.menu_id, m.parent_id, m.menu_name, m.path, m.component, m.visible, m.status, ifnull(m.perms,'') as perms, m.is_frame, m.is_cache, m.menu_type, m.icon, m.order_num, m.create_time
        FROM
            sys_menu m
            LEFT JOIN sys_role_menu rm ON m.menu_id = rm.menu_id
        WHERE
            rm.role_id in (:roleIdList) and
            m.menu_type in ('M', 'C') and 
            m.status = 0
		order by m.parent_id, m.order_num
        `, {
            model: Menu,
            type: QueryTypes.SELECT,
            mapToModel: true,
            replacements: { roleIdList, }
        });

        return results

    },
    async selectMenuList() {
        const results = await sequelize.query(`
        select distinct m.menu_id, m.parent_id, m.menu_name, m.path, m.component, m.visible, m.status, ifnull(m.perms,'') as perms, m.is_frame, m.is_cache, m.menu_type, m.icon, m.order_num, m.create_time
		from sys_menu m 
        where m.menu_type in ('M', 'C','F') and m.status = 0
		order by m.parent_id, m.order_num`, {
            model: Menu,
            type: QueryTypes.SELECT,
            mapToModel: true,
           
        });

        return results

    },
    async selectMenuPermsByUserId(userId) {
        const [results, metadata] = await sequelize.query(`
        select distinct m.perms
		from sys_menu m
			 left join sys_role_menu rm on m.menu_id = rm.menu_id
			 left join sys_user_role ur on rm.role_id = ur.role_id
			 left join sys_role r on r.role_id = ur.role_id
		where m.status = '0' and r.status = '0' and ur.user_id = ${userId}
        `);

        return results

    },
    async selectMenuByroleId(roleId) {

        const results = await sequelize.query(`
        SELECT
            m.menu_id,
            m.menu_name,
            m.parent_id,
            m.menu_type 
        FROM
            sys_menu m
            LEFT JOIN sys_role_menu rm ON m.menu_id = rm.menu_id
        WHERE
            rm.role_id = :roleId
        `, {
            model: Menu,
            type: QueryTypes.SELECT,
            mapToModel: true,
            replacements: { roleId, }
        });

        return results

    },
   
    async selectMenuListByRoleId(roleId, isMenuCheckStrictly) {
        if (typeof isMenuCheckStrictly !== 'boolean') {
            throw 'TypeError:Parameter isMenuCheckStrictly accept only Boolean values'
        }
        let branch = ''
        if (isMenuCheckStrictly) { branch = 'AND m.menu_id NOT IN ( SELECT m.parent_id FROM sys_menu m INNER JOIN sys_role_menu rm ON m.menu_id = rm.menu_id AND rm.role_id = :roleId ) ' }

        results = await sequelize.query(`
        SELECT
            m.menu_id 
        FROM
            sys_menu m
            LEFT JOIN sys_role_menu rm ON m.menu_id = rm.menu_id 
        WHERE
            rm.role_id = :roleId 
        ${branch}
        ORDER BY
            m.parent_id,
            m.order_num
        `, {
            model: Menu,
            type: QueryTypes.SELECT,
            mapToModel: true,
            replacements: { roleId, }
        });
        let newResults = []
        results.forEach((item) => {
            newResults.push(item.menuId)
        })

        return newResults
    }

}