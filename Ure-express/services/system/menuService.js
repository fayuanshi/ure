const menuDal = require('../../dal/system/menuDal')
module.exports = {
    //菜单构建
    buildMenus(menus) {
        //数据库的扁平菜单结构转为树形结构
        function getChildren(parentId) {
            let routers = []
            for (let i = 0; i < menus.length; i++) {
                if (menus[i].parentId == parentId) {
                    if (menus[i].menuType == "C") {
                        routers.push(menus[i])
                    } else if (menus[i].menuType == "M") {
                        menus[i]['children'] = getChildren(menus[i].menuId)
                        routers.push(menus[i])
                    }
                }
            }

            return routers

        }
        let beforeGenarateRoter = getChildren(0)
        //将树形菜单结构过滤为vue-router所需结构
        function genarateRouter(routers) {
            let newRouters = []

            for (let i = 0; i < routers.length; i++) {
                let router = routers[i]

                if (typeof (router.children) === 'undefined') {
                    if (router.parentId === 0) {
                        let newrouter = {
                            component: "Layout",
                            hidden: router.visible === "0" ? false : true,
                            meta: {
                                title: router.menuName,
                                icon: router.icon,
                                noCache: router.isCache === 0 ? false : true,
                            },
                            path: "/",
                            children: [{
                                component: router.component,
                                hidden: router.visible === "0" ? false : true,
                                meta: {
                                    title: router.menuName,
                                    icon: router.icon,
                                    noCache: router.isCache === 0 ? false : true,
                                },
                                name: router.path.slice(0, 1).toUpperCase() + router.path.slice(1),
                                path: router.path,
                            }]

                        }

                        newRouters.push(newrouter)
                    } else {
                        let newrouter = {
                            component: router.component ? router.component : 'Layout',
                            hidden: router.visible === "0" ? false : true,
                            meta: {
                                title: router.menuName,
                                icon: router.icon,
                                noCache: router.isCache === 0 ? false : true,
                            },
                            name: router.path.slice(0, 1).toUpperCase() + router.path.slice(1),
                            path: router.parentId === 0 ? "/" + router.path : router.path,

                        }



                        newRouters.push(newrouter)
                    }


                } else {
                    let newrouter = {
                        alwaysShow: true,
                        component: router.parentId === 0 ? "Layout" : "ParentView",
                        hidden: router.visible === "0" ? false : true,
                        meta: {
                            title: router.menuName,
                            icon: router.icon,
                            noCache: router.isCache === 0 ? false : true,
                        },
                        name: router.path.slice(0, 1).toUpperCase() + router.path.slice(1),
                        path: router.parentId === 0 ? "/" + router.path : router.path,
                        redirect: "noRedirect"
                    }

                    newrouter['children'] = genarateRouter(router.children)

                    newRouters.push(newrouter)
                }
            }

            return newRouters

        }

        let newrouters = genarateRouter(beforeGenarateRoter)

        for (let i = newrouters.length; i > 0; i--) {
            newrouters[i] = newrouters[i - 1]
        }

        newrouters[0] = {
            "path": "/",
            "hidden": true,
            "component": "Layout",
            "meta": {
                "title": "待办事项",
                "icon": "#",
                "noCache": false
            },
            "children": [{
                "name": "Backlog",
                "path": "backlog",
                "hidden": false,
                "component": "backlog/backlog",
                "meta": {
                    "title": "待办事项",
                    "icon": "#",
                    "noCache": false
                }
            }]
        }



        return newrouters

    },
    async getPerms(user) {

        if (user.admin) {
            return ['*:*:*']
        }

        const perms = await menuDal.selectMenuPermsByUserId(user.userId)
        let newPerms = []
        for (let i = 0; i < perms.length; i++) {
            if (typeof (perms[i].perms) == 'string' && perms[i].perms.length !== 0) {
                newPerms.push(perms[i].perms)
            }

        }

        return newPerms


    },
    async getTreeselect(menus) {

        function getChildren(parentId) {
            let newMenus = []
            for (let i = 0; i < menus.length; i++) {
                if (menus[i].parentId == parentId) {
                    if (menus[i].menuType == "F") {
                        let menu = {
                            id: menus[i].menuId,
                            label: menus[i].menuName
                        }
                        newMenus.push(menu)

                    } else if (menus[i].menuType == "M" || menus[i].menuType == "C") {

                        let menu = {
                            id: menus[i].menuId,
                            label: menus[i].menuName,
                            children: getChildren(menus[i].menuId)
                        }
                        newMenus.push(menu)
                    }
                }
            }

            return newMenus

        }

        let treeselect = getChildren(0)

        return treeselect
    }


}