
const roleDal = require('../../dal/system/roleDal')
const Dept = require('../../models/system/dept')

module.exports = {
    //获取完整用户信息
    async getUser(user){
        
        
        const roleList = await roleDal.selectRoleListByUserId(user.userId)
        const roles = []
        roleList.forEach(async (item)=>{
            const role = await roleDal.selectRoleById(item.role_id)
            roles.push(role)
        })
        const dept = await Dept.findByPk(user.deptId)
        user['roles'] = roles
        if(dept){
            user['dept'] = dept.dataValues
        }
        


        return user

    }, 
    






}