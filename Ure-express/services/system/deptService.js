const Dept = require('../../models/system/dept')
module.exports = {
    buildDeptTree(deptList) {

        function getChildren(parentId) {
            let deptsTree = []
            deptList.forEach((item) => {
                if (item.parentId == parentId) {
                    let dept = {
                        id: item.deptId,
                        label: item.deptName,
                        children: getChildren(item.deptId)
                    }
                    deptsTree.push(dept)
                }
            })
            return deptsTree
        }

        function removeEmpty(deptTreeNode) {

            if (deptTreeNode['children'].length == 0) {

                delete deptTreeNode['children']
            } else {

                for (let i = 0; i < deptTreeNode['children'].length; i++) {
                    removeEmpty(deptTreeNode['children'][i])
                }
            }
        }

        let deptTree = getChildren(0)

        removeEmpty(deptTree[0])

        return deptTree


    },
    async buildAncestors(id) {
        let result
        async function findParentId(deptId, ancestors) {
            let dept = await Dept.findByPk(deptId)
            dept = JSON.parse(JSON.stringify(dept))
            if (dept.parentId == 0) {
                if (ancestors === '') {
                    newAncestors = dept.parentId
                } else {
                    newAncestors = dept.parentId + ',' + ancestors
                }

                console.log('final', newAncestors);
                result = newAncestors

            } else {
                let newAncestors
                if (ancestors === '') {
                    newAncestors = dept.parentId

                } else {
                    newAncestors = dept.parentId + ',' + ancestors

                }
                await findParentId(dept.parentId, newAncestors)
                console.log('jx', newAncestors);
            }
        }
        await findParentId(id, '')
        return result

    }
}