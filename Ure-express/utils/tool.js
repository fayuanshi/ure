module.exports = {
    //下划线分割转小驼峰
    underlineToHump(str){
        let a = str.split("_");
        let result = a[0];
        for(var i=1;i<a.length;i++){
            result = result + a[i].slice(0,1).toUpperCase() + a[i].slice(1);
        }
        return result
    },
    //小驼峰转下划线
    humpToUnderline(str){
        return str.replace(/([A-Z])/g,"_$1").toLowerCase()
    }
}