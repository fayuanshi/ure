module.exports = (app)=>{

    let date = new Date();
    let Y = date.getFullYear()
    let M = date.getMonth()+1
    let D = date.getDate()

    return `${app.locals.uploadPath}/${Y}/${M}/${D}`

}