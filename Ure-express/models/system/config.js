const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../../framework/db')

const Model = sequelize.define('sys_config', {
	configId: { type: DataTypes.INTEGER, primaryKey: true,field: 'config_id',autoIncrement: true  },
	configName: { type: DataTypes.STRING,field: 'config_name', },
    configKey: { type: DataTypes.STRING,field: 'config_key', },
    configValue: { type: DataTypes.STRING,field: 'config_value', },
    configType: { type: DataTypes.STRING,field: 'config_type', },
	createBy: { type: DataTypes.STRING,field: 'create_by' },
	createTime: { type: DataTypes.DATE,field: 'create_time' },
	updateBy: { type: DataTypes.STRING,field: 'update_by' },
	updateTime: { type: DataTypes.DATE,field: 'update_time' },
},
	{
		freezeTableName: true,
		timestamps: false
	}
);



module.exports = Model