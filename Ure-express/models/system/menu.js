const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../../framework/db')

const Model = sequelize.define('sys_menu', {
	menuId: { type: DataTypes.INTEGER, primaryKey: true,field: 'menu_id',autoIncrement: true  },
	menuName: { type: DataTypes.STRING,field: 'menu_name', },
    parentId: { type: DataTypes.INTEGER, field: 'parent_id' },
    orderNum: { type: DataTypes.INTEGER, field: 'order_num' },
    path: { type: DataTypes.STRING,field: 'path', },
    component: { type: DataTypes.STRING,field: 'component', },
    isFrame: { type: DataTypes.INTEGER, field: 'is_frame' },
    isCache: { type: DataTypes.INTEGER, field: 'is_cache' },
    menuType: { type: DataTypes.STRING,field: 'menu_type', },
    visible: { type: DataTypes.STRING,field: 'visible', },
    status: { type: DataTypes.STRING,field: 'status', },
    perms: { type: DataTypes.STRING,field: 'perms', },
    icon: { type: DataTypes.STRING,field: 'icon', }, 
	createBy: { type: DataTypes.STRING,field: 'create_by' },
	createTime: { type: DataTypes.DATE,field: 'create_time' }, 
	updateBy: { type: DataTypes.STRING,field: 'update_by' },
	updateTime: { type: DataTypes.DATE,field: 'update_time' },
    remark: { type: DataTypes.STRING,field: 'remark'},
},
	{
		freezeTableName: true,
		timestamps: false
	}
);



module.exports = Model