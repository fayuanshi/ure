const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../../framework/db')

const Model = sequelize.define('sys_role', {
	roleId: { type: DataTypes.INTEGER, primaryKey: true,field: 'role_id' ,autoIncrement: true},
	roleName: { type: DataTypes.STRING,field: 'role_name' },
	roleKey: { type: DataTypes.STRING,field: 'role_key' },
    roleSort: { type: DataTypes.INTEGER,field: 'role_sort' },
	dataScope: { type: DataTypes.STRING,field: 'data_scope' },
    menuCheckStrictly: { type: DataTypes.INTEGER,field: 'menu_check_strictly' },
    deptCheckStrictly: { type: DataTypes.INTEGER,field: 'dept_check_strictly' },
	status: { type: DataTypes.STRING },
	delFlag: { type: DataTypes.STRING,field: 'del_flag' },
	createBy: { type: DataTypes.STRING,field: 'create_by' },
	createTime: { type: DataTypes.DATE,field: 'create_time' },
	updateBy: { type: DataTypes.STRING,field: 'update_by' },
	updateTime: { type: DataTypes.DATE,field: 'update_time' },
	remark: { type: DataTypes.STRING},

},
	{
		freezeTableName: true,
		timestamps: false
	}
);

// (async () => {
//   await sequelize.sync();
//   // 这里是代码
// })();

module.exports = Model