const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../../framework/db')

const Model = sequelize.define('sys_user_role', {
	userId: { type: DataTypes.INTEGER, primaryKey: true,field: 'user_id'},
    roleId: { type: DataTypes.INTEGER, primaryKey: true,field: 'role_id'},


},
	{
		freezeTableName: true,
		timestamps: false
	}
);

module.exports = Model