const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../../framework/db')

const Model = sequelize.define('sys_dept', {
	deptId: { type: DataTypes.INTEGER, primaryKey: true,field: 'dept_id',autoIncrement: true},
	parentId: { type: DataTypes.INTEGER,field: 'parent_id'  },
	ancestors: { type: DataTypes.STRING },
	deptName: { type: DataTypes.STRING,field: 'dept_name', },
	orderNum: { type: DataTypes.INTEGER,field: 'order_num' },
	leader: { type: DataTypes.STRING },
	phone: { type: DataTypes.STRING },
	email: { type: DataTypes.STRING },
	status: { type: DataTypes.STRING },
	delFlag: { type: DataTypes.STRING,field: 'del_flag' },
	createBy: { type: DataTypes.STRING,field: 'create_by' },
	createTime: { type: DataTypes.DATE,field: 'create_time' },
	updateBy: { type: DataTypes.STRING,field: 'update_by' },
	updateTime: { type: DataTypes.DATE,field: 'update_time' },
	

},
	{
		freezeTableName: true,
		timestamps: false
	}
);

// (async () => {
//   await sequelize.sync();
//   // 这里是代码
// })();

module.exports = Model