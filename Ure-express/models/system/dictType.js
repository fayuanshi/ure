const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../../framework/db')

const Model = sequelize.define('sys_dict_type', {
	dictId: { type: DataTypes.INTEGER, primaryKey: true,field: 'dict_id',autoIncrement: true  },
	dictName: { type: DataTypes.INTEGER,field: 'dict_name'  },
    dictType: { type: DataTypes.STRING,field: 'dict_type', },
	status: { type: DataTypes.STRING },
	delFlag: { type: DataTypes.STRING,field: 'del_flag' },
	createBy: { type: DataTypes.STRING,field: 'create_by' },
	createTime: { type: DataTypes.DATE,field: 'create_time' },
	updateBy: { type: DataTypes.STRING,field: 'update_by' },
	updateTime: { type: DataTypes.DATE,field: 'update_time' },
	

},
	{
		freezeTableName: true,
		timestamps: false
	}
);


module.exports = Model