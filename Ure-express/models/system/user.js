const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../../framework/db')

const Model = sequelize.define('sys_user', {
	userId: { type: DataTypes.INTEGER, primaryKey: true,field: 'user_id',autoIncrement: true },
	deptId: { type: DataTypes.INTEGER,field: 'dept_id' },
	userName: { type: DataTypes.STRING,field: 'user_name' },
	nickName: { type: DataTypes.STRING,field: 'nick_name' },
	userType: { type: DataTypes.STRING,field: 'user_type' },
	email: { type: DataTypes.STRING },
	phonenumber: { type: DataTypes.STRING },
	sex: { type: DataTypes.STRING },
	avatar: { type: DataTypes.STRING },
	password: { type: DataTypes.STRING },
	status: { type: DataTypes.STRING },
	delFlag: { type: DataTypes.STRING,field: 'del_flag' },
	loginIp: { type: DataTypes.STRING,field: 'login_ip' },
	loginDate: { type: DataTypes.DATE,field: 'login_date' },
	createBy: { type: DataTypes.STRING,field: 'create_by' },
	createTime: { type: DataTypes.DATE,field: 'create_time' },
	updateBy: { type: DataTypes.STRING,field: 'update_by' },
	updateTime: { type: DataTypes.DATE,field: 'update_time' },
	remark: { type: DataTypes.STRING},

},
	{
		freezeTableName: true,
		timestamps: false
	}
);

module.exports = Model