const{Sequelize,DataTypes} = require('sequelize');
const sequelize = require('../../framework/db');

const Model = sequelize.define('sys_notice',{
    noticeId:{type:DataTypes.INTEGER,primaryKey:true,field:'notice_id',autoIncrement:true},
    noticeTitle:{type:DataTypes.STRING,field:'notice_title'},
    noticeType:{type:DataTypes.STRING,field:'notice_type'},
    noticeContent:{type:DataTypes.BLOB,field:'notice_content'},
    status:{type:DataTypes.STRING,field:'status'},
    createTime: { type: DataTypes.DATE,field: 'create_time' },
    updateBy:{type:DataTypes.STRING,field:'update_by'},
    updateTime: { type: DataTypes.DATE,field: 'update_time' },
    remark:{type:DataTypes.STRING,field:'remark'},
    
},
    {
        freezeTableName: true,
        timestamps: false
    }
);

module.exports = Model