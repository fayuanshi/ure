const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../../framework/db')

const Model = sequelize.define('sys_dict_data', {
	dictCode: { type: DataTypes.INTEGER, primaryKey: true,field: 'dict_code',autoIncrement: true  },
	dictSort: { type: DataTypes.INTEGER,field: 'dict_sort'  },
	dictLabel: { type: DataTypes.STRING,field: 'dict_label', },
    dictValue: { type: DataTypes.STRING,field: 'dict_value', },
    dictType: { type: DataTypes.STRING,field: 'dict_type', },
    cssClass: { type: DataTypes.STRING,field: 'css_class', },
    listClass: { type: DataTypes.STRING,field: 'list_class', },
    isDefault: { type: DataTypes.STRING,field: 'is_default', },
	status: { type: DataTypes.STRING },
	createBy: { type: DataTypes.STRING,field: 'create_by' },
	createTime: { type: DataTypes.DATE,field: 'create_time' },
	updateBy: { type: DataTypes.STRING,field: 'update_by' },
	updateTime: { type: DataTypes.DATE,field: 'update_time' },
	

},
	{
		freezeTableName: true,
		timestamps: false
	}
);


module.exports = Model