const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../../framework/db')

const Model = sequelize.define('sys_post', {
	postId: { type: DataTypes.INTEGER, primaryKey: true,field: 'post_id' ,autoIncrement: true},
	postCode: { type: DataTypes.STRING,field: 'post_code' },
    postName: { type: DataTypes.STRING,field: 'post_name' },
    postSort: { type: DataTypes.INTEGER,field: 'post_sort' },
	status: { type: DataTypes.STRING },
	createBy: { type: DataTypes.STRING,field: 'create_by' },
	createTime: { type: DataTypes.DATE,field: 'create_time' },
	updateBy: { type: DataTypes.STRING,field: 'update_by' },
	updateTime: { type: DataTypes.DATE,field: 'update_time' },
	remark: { type: DataTypes.STRING},

},
	{
		freezeTableName: true,
		timestamps: false
	}
);



module.exports = Model