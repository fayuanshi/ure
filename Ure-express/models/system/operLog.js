const{Sequelize,DataTypes} = require('sequelize');
const sequelize = require('../../framework/db');

const Model = sequelize.define('sys_oper_log',{
    operId:{type:DataTypes.INTEGER,primaryKey:true,field:'oper_id',autoIncrement:true},
    title:{type:DataTypes.STRING,field:'title'},
    bussinessType:{type:DataTypes.INTEGER,field:'bussiness_type'},
    method:{type:DataTypes.STRING,field:'method'},
    requestMethod:{type:DataTypes.STRING,field:'request_method'},
    operatorType:{type:DataTypes.INTEGER,field:'operator_type'},
    operName:{type:DataTypes.STRING,field:'oper_name'},
    deptName:{type:DataTypes.STRING,field:'dept_name'},
    operUrl:{type:DataTypes.STRING,field:'oper_url'},
    operIp:{type:DataTypes.STRING,field:'oper_ip'},
    operLocation:{type:DataTypes.STRING,field:'oper_location'},
    operParam:{type:DataTypes.STRING,field:'oper_param'},
    jsonResult:{type:DataTypes.STRING,field:'json_result'},
    status:{type:DataTypes.INTEGER,field:'status'},
    errorMsg:{type:DataTypes.STRING,field:'error_msg'},
    operTime: { type: DataTypes.DATE,field: 'oper_time' },
},
    {
        freezeTableName: true,
        timestamps: false
    }
);

module.exports = Model