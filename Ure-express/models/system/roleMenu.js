const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../../framework/db')

const Model = sequelize.define('sys_role_menu', {
    roleId: { type: DataTypes.INTEGER, primaryKey: true,field: 'role_id'},
    menuId: { type: DataTypes.INTEGER, primaryKey: true,field: 'menu_id'},


},
	{
		freezeTableName: true,
		timestamps: false
	}
);

module.exports = Model