const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../../framework/db')

const Model = sequelize.define('sys_user_post', {
	userId: { type: DataTypes.INTEGER, primaryKey: true,field: 'user_id'},
    postId: { type: DataTypes.INTEGER, primaryKey: true,field: 'post_id'},


},
	{
		freezeTableName: true,
		timestamps: false
	}
);

module.exports = Model