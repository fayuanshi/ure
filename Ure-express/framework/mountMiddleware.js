module.exports = async (app) => {
    const path = require('path');
    const { readdir } = require('fs/promises')
    let middlewarePath = path.join(__dirname, '../middleware')
    let mw = {}

    try {
        const files = await readdir(middlewarePath);
        for (const file of files){
            const middlewareFullPath = path.join(middlewarePath,file)
            mw[file.slice(0,-3)]= require(middlewareFullPath)
        }

        app.locals.middleware = mw
    } catch (err) {
        console.error(err);
    }



}