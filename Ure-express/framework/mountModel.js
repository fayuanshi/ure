module.exports = async (app) => {
    const path = require('path');
    const { readdir } = require('fs/promises')
    let modelPath = path.join(__dirname, '../models')
    let models = {}

    try {

        async function mount(mountPath) {
            const dirList = await readdir(mountPath)
            for (const item of dirList) {
                if (item.substring(item.length - 3) === '.js') {
                    const modelFullPath = path.join(mountPath, item)
                    let modelName = item.slice(0, -3)
                    modelName = modelName.charAt(0).toUpperCase() + modelName.slice(1); 
                    models[modelName] = require(modelFullPath)
                } else {
                    const dirPath = path.join(mountPath, item)
                    await mount(dirPath)
                }
            }

        }

        await mount(modelPath)
        app.locals.models = models
      
    } catch (err) {
        console.error(err);
    }



}