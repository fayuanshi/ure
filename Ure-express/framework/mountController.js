module.exports = async (app) => {
    const path = require('path');
    const { readdir } = require('fs/promises')
    let controllerPath = path.join(__dirname, '../controllers')



    async function mount(mountPath){
        const dirList = await readdir(mountPath)
        for(const item of dirList){
            if(item.substring(item.length-3) ==='.js'){
                const controllerFullPath = path.join(mountPath,item)
                require(controllerFullPath)(app)
            }else{
                const dirPath = path.join(mountPath,item)
                await mount(dirPath)
            }
        }
  
    }

    await mount(controllerPath)

}