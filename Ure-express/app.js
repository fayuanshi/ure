const express = require('express')
const app = express()
const cors = require('cors')
const jwt = require('jsonwebtoken')
const path = require('path')
const mountController = require('./framework/mountController')
const mountMiddleware = require('./framework/mountMiddleware')
const mountModel = require('./framework/mountModel')


require('./framework/db.js')
app.locals.uploadPath = '/Users/deerface/ure/upload'
app.use(cors())
app.use(express.json())
app.use(express.static('public'))
app.use(express.static(app.locals.uploadPath ))

mountMiddleware(app)
mountModel(app)
setTimeout(()=>{
    mountController(app)
},500)

 
 





app.listen('8080', () => {
    console.log('HTTP SERVER LISTENING ON :8080')
})

module.exports = app